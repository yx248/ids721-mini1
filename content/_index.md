+++
title = "Home Page"
template = "index.html"
+++

## IDS-271 Course Description

This course offers a comprehensive exploration of cloud computing, delving into Big Data, Machine Learning, and Large Language Models (LLMs). Interactive AWS labs provide a hands-on learning experience. The curriculum is project-based, ensuring practical application of concepts learned.

## Learning Objectives

Upon successful completion of this course, you will be able to:

- Summarize the fundamentals of cloud computing.
- Achieve AWS Solutions Architect Certification.
- Master the Rust language for general programming.
- Effectively utilize AI Pair Programming.
- Evaluate the economics of cloud computing.
- Address distributed computing challenges and opportunities.
- Foster non-linear, life-long learning skills.
- Craft compelling portfolios with GitLab, Hugging Face, YouTube, LinkedIn, and a personal website.
- Enhance Metacognition skills through teaching.

## Core Tech Stack

Students will gain experience with an array of technologies, including:

- Rust programming language.
- AWS, including AWS Learner Labs and optional Free Tier account.
- Version control with GitLab.
- Collaboration via Slack.
- Computing resources like AWS Lightsail for GPU-intensive tasks.
- CodeWhisperer for AI-assisted coding.

## Communication

Effective communication is facilitated through:

- Slack for day-to-day communication.
- GitLab for code management and review.

## Media and Labs

Students will engage with:

- Duke AIML Group's resources on GitLab.
- A comprehensive series, "52 Weeks of AWS".
- AWS Academy Labs and sandbox environments.
- Research computing on AWS Lightsail (subject to availability).
- Relevant Coursera courses.

## Reading Material

The course includes a curated selection of readings:

- "Developing on AWS with C#"
- "Implementing MLOps in the Enterprise"
- "Practical MLOps"
- "Python for DevOps"
- "The Rust Programming Language" (online book)
- "Know Thyself: The Science of Self-Awareness"
- A collection of AWS Whitepapers and Certification study materials.
